import requests
import sqlite3

from models import Sport, Event, CustomFactor

# Входные данные
SOURCE_URL = 'https://line16.bkfon-resource.ru/live/currentLine/ru/'
CATEGORY_IDS = [29086, 40479, 40480, 40481, 44943, 45827]


if __name__ == '__main__':
    # Извлекаем данные
    r = requests.get(SOURCE_URL)
    raw_data = r.json()

    # Переносим сырые данные в модели
    for sport_dict in raw_data['sports']:
        Sport(sport_dict)
    for event_dict in raw_data['events']:
        Event(event_dict)
    for custom_factor_dict in raw_data['customFactors']:
        CustomFactor(custom_factor_dict)

    # Запрашиваем обработанные данные
    events = []
    for category_id in CATEGORY_IDS:
        events.extend(Event.get_events_by_category(category_id))

    # Сохраняем обработанные данные в SQLite
    conn = sqlite3.connect('parsed_data.db')
    cur = conn.cursor()

    cur.execute("""CREATE TABLE IF NOT EXISTS events (
                    Id_fon INTEGER PRIMARY KEY,
                    Category TEXT,
                    Event_name TEXT,
                    Gamer TEXT,
                    Kof TEXT
                   )""")

    for event in events:
        cur.execute("INSERT INTO events VALUES (?, ?, ?, ?, ?)", event.dump())

    conn.commit()
    cur.close()

    print('Done')
