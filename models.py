"""
Модели для хранения и обработки полученных данных.
"""


class Sport:
    """
    Модель категории/турнира.
    """
    _categories = {}
    _tournaments = {}

    def __init__(self, source_dict):
        self.id = source_dict['id']
        self.name = source_dict['name']

        if 'parentId' in source_dict:  # Если есть parentId - турнир, если нет - категория
            self.parent_id = source_dict['parentId']
            self.is_category = False

            __class__._tournaments[self.id] = self
        else:
            self.is_category = True

            __class__._categories[self.id] = self

    @staticmethod
    def get_tournaments_by_category(category_id):
        """
        Извлекаем словарь с турнирами по категории (в качестве ключей - ID турнира).
        :param category_id: ID категории
        :return: Словарь, содержащий турниры и их ID
        """
        return __class__._categories[category_id].name, \
            {tid: tournament for
                tid, tournament in __class__._tournaments.items() if tournament.parent_id == category_id}

    @staticmethod
    def get_sport_name_by_id(sport_id):
        return __class__._tournaments[sport_id].name if sport_id in __class__._tournaments \
            else __class__._categories[sport_id].name


class Event:
    """
    Модель события/части события (часть события - сет, тайм, т.д.)
    """
    _events = {}
    _event_parts = {}

    def __init__(self, source_dict):
        self.id = source_dict['id']
        self.sport_id = source_dict['sportId']

        if 'parentId' in source_dict:  # Если есть parentId - часть события, если нет - событие
            self.parent_id = source_dict['parentId']
            self.is_part = True

            __class__._event_parts[self.id] = self
        else:
            self.team_1 = source_dict['team1']
            self.team_2 = source_dict['team2'] if 'team2' in source_dict else None  # В редких случаях может быть указана только одна команда
            self.is_part = False

            __class__._events[self.id] = self

    @staticmethod
    def get_events_by_category(category_id):
        """
        Основной метод извлечения данных.
        Опрашивает все остальные модели, комбинируя нужные данные из них.
        :param category_id: ID категории
        :return: Список объектов EventCombined (о них ниже)
        """
        try:
            category_name, tournaments = Sport.get_tournaments_by_category(category_id)
        except KeyError:
            print(f'Категория с ID {category_id} не найдена.')
            return []
        ids_full = list(tournaments.keys())  # ID всех событий в данной категории
        ids_full.append(category_id)

        events = {event.id: EventCombined(event, category_name, Sport.get_sport_name_by_id(event.sport_id),
                                          CustomFactor.get_factors_by_event(event.id)) for
                  event in __class__._events.values() if event.sport_id in ids_full}  # Извлекаем только события

        third_level_parts = []
        for event_part in __class__._event_parts.values():  # Добавляем коэффициенты частей событий к соответствующим событиям
            if event_part.sport_id in ids_full:
                coefs = CustomFactor.get_factors_by_event(event_part.id)
                events[event_part.parent_id].kof.extend(coefs)

        """
        В редких случаях могут встречаться части третьего уровня - части событий,
        ссылающиеся на другие части (например, угловые). Их необходимо обработать отдельно.
        """
        for third_level_part in third_level_parts:
            coefs = CustomFactor.get_factors_by_event(third_level_part.id)
            events[__class__._event_parts[third_level_part.parent_id].parent_id].kof.extend(coefs)

        return list(events.values())


class CustomFactor:
    """
    Модель для коэффициентов.
    """
    _custom_factors = []

    def __init__(self, source_dict):
        self.event_id = source_dict['e']
        self.coef_id = source_dict['f']
        self.additional = source_dict['pt'] if 'pt' in source_dict else None
        self.coef = source_dict['v']

        __class__._custom_factors.append(self)

    def __repr__(self):
        """
        Метод, выводящий форматированный коэффициент.
        :return: str
        """
        additional = self.additional if self.additional else ''
        return f'{self.coef_id}|,|{additional}|,|{self.coef}|;|'

    @staticmethod
    def get_factors_by_event(event_id):
        return [factor for factor in __class__._custom_factors if factor.event_id == event_id]


class EventCombined:
    """
    Модель комбинированного события, готового к выводу.
    Включает в себя все выходные данные (ID события, категорию, название турнира,
    команды/игроков и список всех коэффициентов (включая части события).
    """
    def __init__(self, event, category, tournament, coefs):
        self.id_fon = event.id
        self.category = category
        self.tournament = tournament
        self.teams = f'{event.team_1} - {event.team_2}' if event.team_2 else event.team_1
        self.kof = coefs

    def dump(self):
        """
        Преобразует объект в кортеж, готовый к сохранению в БД.
        :return: tuple
        """
        return self.id_fon, self.category, self.tournament, self.teams, ''.join([repr(coef) for coef in self.kof])
